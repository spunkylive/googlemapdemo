class User < ActiveRecord::Base

  geocoded_by :full_address
  validates :name, :address, :country, presence: true
  validates :city, presence: true, if: :validate_city?

  after_validation :validate_address
  after_validation :geocode

  scope :get_users, -> (ids) { where(id: ids) }

  def validate_address
    user_address = address
    user_address = "#{user_address}, #{city_name}" if city.present?
    user_address = "#{user_address}, #{state_name}" if state
    user_address = "#{user_address}, #{country_name}" if country
    self.full_address = user_address
  end


  def validate_city?
    country_code = ['VG', 'VA', 'UM', 'TC', 'SX', 'SG', 'RE', 'PW', 'PN', 'PM', 'NU', 'NF', 'MQ', 'MO', 'MH', 'MF', 'MC', 'KY', 'IO', 'IM',
                    'GS', 'GP', 'GI', 'GF', 'FM', 'FK', 'DJ', 'CX', 'CW', 'CK', 'CC', 'BL', 'AW', 'AQ', 'AI']
    !country_code.include? country
  end


  def state_name
    CS.states(country.to_sym)[state.to_sym] if state.present?
  end


  def country_name
    CS.countries[country.to_sym]
  end

  def city_name
    city.split('_').map(&:capitalize).join(' ')
  end

end
