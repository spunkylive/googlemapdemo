class UsersController < ApplicationController

  before_action :find_user, only: [:edit, :update, :destroy]

  def new
    @user = User.new
  end


  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to root_path
    else
      flash[:notice] = find_error(@user)
      flash[:level] = 'ERROR'
      render :new
    end
  end


  def edit
  end


  def update
    if @user.update_attributes(user_params)
      flash[:notice] = 'User Updated successfully'
      flash[:level] = 'SUCCESS'
      redirect_to root_path
    end
  end


  def destroy
    @user.destroy
    flash[:notice] = 'User Deleted successfully'
    flash[:level] = 'SUCCESS'
    redirect_to root_path
  end


  private


  def find_user
    @user = User.find(params[:id])
  end


  def user_params
    params.require(:user).permit(:name, :address, :country, :city, :state)
  end
end
