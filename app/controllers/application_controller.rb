class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :authenticate


  def find_error(obj)
      key = obj.errors.messages.keys.first.to_s
      obj.errors.messages.stringify_keys[key].first
  end

  protected

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == "GuestUser12" && password == "Welcome1!"
    end
  end

end
