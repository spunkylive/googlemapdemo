class HomeController < ApplicationController

  def index
    @users = User.all
    @user = @users.first
  end

  def gmap
    @user = User.find params[:id]
  end

  def get_distance
    ids = params[:ids].split(',')
    users = User.where(id: ids)
    if users.first.country == users.last.country
      render json: {
                 origin: users.first.full_address,
                 destination: users.last.full_address,
                 distance: users.first.distance_from([users.last.latitude, users.last.longitude]).round(2)
             }
    else
      render json: {error: 'Cannot get distance of two different Countries'}
    end

  end

  def distance_from
    @user = User.find params[:id]
    @users = User.all
  end

end
