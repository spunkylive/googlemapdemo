require 'city-state'
class LocationsController < ApplicationController

  def states
    states_data = {}
    if params[:country_id]
      states = CS.states(params[:country_id])
      states_data = states.to_a.map{ |state| [state[1], state[0].to_s]}
    end
    render json: {states: states_data}
  end


  def cities
    cities_data = {}
    if params[:state_id]
      cities = CS.cities(params[:state_id])
      cities_data = cities.map { |city| [city, city.downcase.split(' ').join('_')]}
    end
    render json: {cities: cities_data}
  end

end
