// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .


$(function() {
    show_notification(notifyMessage, notifyLevel);
});


function show_notification(notifyMessage,notifyLevel) {
    if(notifyMessage) {
        var style = null,
            caption = null;

        if(notifyLevel == "ERROR") {
            style = {background: '#da1010', color: '#ffffff'};
            caption = "Error";
        }
        else if(notifyLevel == "INFO") {
            style = {background: '#fa6800', color: '#ffffff'};
            caption = "Info";
        }
        else{
            style = {background: '#145814', color: '#ffffff'};
            caption = "Success";
        }
        $.Notify({
            caption: caption,
            content: notifyMessage,
            style: style,
            timeout: 5000
        });
    }
}


function showDialog(id){
    var dialog = $("#"+id).data('dialog');
    if (!dialog.element.data('opened')) {
        dialog.open();
    } else {
        dialog.close();
    }
}