function getStates() {
    var countryCode = $('#user_country :selected').val();
    $.ajax({
        url: '/locations/'+ countryCode +'/states',
        dataType: "json",
        success: function(data) {
            $('#user_city').html('');
            $('#user_city').append("<option value=''>Select City</option>");
            $('#user_state').html('');
            $('#user_state').append("<option value=''>Select State</option>");
            for (state in data.states)
                if (data.states[state][1] != undefined)
                    $('#user_state').append("<option value=\""+data.states[state][1]+"\">"+data.states[state][0]+"</option>");

        }
    });
}


function getSelectedStates(selectedState) {
    var userSelectedState = '';
    var countryCode = $('#user_country :selected').val();

    if (selectedState != '' && selectedState != undefined )
        userSelectedState = selectedState;
    if (countryCode != '' && countryCode != undefined) {
        $.ajax({
            url: '/locations/'+ countryCode +'/states',
            dataType: "json",
            success: function(data) {
                $('#user_city').html('');
                for (state in data.states) {
                    var stateCode = data.states[state][1];
                    var stateName = data.states[state][0];
                    if (stateCode != undefined) {
                        if (userSelectedState == stateName)
                            $('#user_state').append("<option value=\""+stateCode+"\"selected='selected'>"+stateName+"</option>");
                        else
                            $('#user_state').append("<option value=\""+stateCode+"\">"+stateName+"</option>");
                    }
                }
            }
        });
    }
}


function getCities() {
    var stateCode = $('#user_state :selected').val();
    $.ajax({
        url: '/locations/'+ stateCode +'/cities',
        dataType: "json",
        success: function(data) {
            $('#user_city').html('');
            $('#user_city').append("<option value=''>Select City</option>");
            for (city in data.cities)
                if (data.cities[city][1] != undefined)
                    $('#user_city').append("<option value=\""+data.cities[city][1]+"\">"+data.cities[city][0]+"</option>");
        }
    });
}


function getSelectedCities(selectedCity, stateCode) {
    console.log(selectedCity, stateCode)
    var userSelectedCity = '';
    if (selectedCity != '' && selectedCity != undefined )
        userSelectedCity = selectedCity;

    if (stateCode != '' && stateCode != undefined) {
        $.ajax({
            url: '/locations/'+ stateCode +'/cities',
            dataType: "json",
            success: function(data) {
                $('#user_city').html('');
                for (city in data.cities){
                    var cityCode = data.cities[city][1];
                    var cityName = data.cities[city][0];
                    if (cityCode != undefined) {
                        if (userSelectedCity == cityName){
                            $('#user_city').append("<option value=\""+cityCode+"\"selected='selected'>"+cityName+"</option>");
                        }
                        else
                            $('#user_city').append("<option value=\""+cityCode+"\">"+cityName+"</option>");
                    }

                }
            }
        });
    }
}


