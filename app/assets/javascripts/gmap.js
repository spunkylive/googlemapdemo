var marker;
var coords;
var user_ids = [];
var distance_map;
var map;

$(document).ready(function() {
    $('#distance').bind('click', function() {
        var url = "home/get_distance?ids=" + user_ids ;
        if (user_ids.length == 2) {
            $.ajax({
                type: "GET",
                url: url,
                dataType: "json",
                success: function (result) {
                    user_ids = [];

                    if (result.error == undefined) {
                        var directionsService = new google.maps.DirectionsService();
                        var directionsDisplay = new google.maps.DirectionsRenderer();
                        showDialog('dialog7');
                        $('.graph_heading').text('Distance');
                        var paragraphText = ' From  <strong>' + result.origin + '</strong>  To  <strong>' + result.destination +
                            '</strong> is <strong>' + result.distance + ' miles </strong>';
                        $('.graph_para').html(paragraphText);
                        distance_map = new google.maps.Map(document.getElementById("map"));
                        directionsDisplay.setMap(distance_map);
                        var request = {
                            origin: result.origin,
                            destination: result.destination,
                            travelMode: google.maps.TravelMode.DRIVING
                        };

                        directionsService.route(request, function(response, status) {
                            //Check if request is successful.
                            if (status == google.maps.DirectionsStatus.OK) {
                                directionsDisplay.setDirections(response); //Display the directions result
                            }
                        });

                    }
                    else {
                        show_notification(result.error, 'ERROR');
                        $('.source').addClass('bg-cyan').text('Choose');
                        $('.destination').addClass('bg-cyan').text('Choose');
                    }
                },
                error: function () {
                    show_notification('something wrong!', 'ERROR');
                }
            });
        }
        else
            show_notification('Select 2 locations', 'ERROR');
        return false;
    });
});


$(document).on('click', '.compare', function() {
    if (user_ids.length >= 2)
        show_notification('Can not find distance more than 2', 'ERROR');
    else {
        if (user_ids.length == 0) {
            $('.source').addClass('bg-cyan').removeClass('bg-green').text('Choose');
            $('.destination').addClass('bg-cyan').removeClass('bg-red').text('Choose');
        }
        $(this).removeClass('bg-cyan');
        user_ids.push($(this).attr('rel'));
        if(user_ids.length == 1) {

            $(this).text('Source');
            $(this).addClass('bg-green source');
        }
        else if (user_ids.length == 2) {
            if (user_ids[0] == user_ids[1]) {
                show_notification('Source & destination can not be same', 'ERROR');
                user_ids = [];
                $('.source').addClass('button_shadow blue').text('Choose');
                $('.destination').addClass('button_shadow blue').text('Choose');
            }
            else {
                $(this).text('Destination');
                $(this).addClass('bg-red destination');
            }
        }
    }
    return false;
});


function createMarker(coords, static_map){
    addr = coords.lat.toString() + ' ' + coords.lng.toString();
    marker = new google.maps.Marker({
        position: coords,
        map: static_map
    });
}


function initMap(latitude, longitude) {
    coords = {lat: latitude, lng: longitude};
    map = new google.maps.Map(document.getElementById('map'), {
        center: coords,
        zoom: 18
    });
    createMarker(coords, map);
}







