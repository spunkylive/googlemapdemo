Rails.application.routes.draw do

  resources(:locations, only: []) do

    collection do
      get(':country_id/states', action: :states)
      get(':state_id/cities', action: :cities)
    end

  end


  resources(:users)


  resources(:home, only: :index) do

    collection do
      get(:gmap)
      get(:get_distance)
    end

    member do
      get(:distance_from)
    end

  end

  root 'home#index'
end
